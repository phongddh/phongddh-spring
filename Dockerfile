FROM openjdk:17

WORKDIR /app
COPY ${JAR_FILE} app.jar 
ENTRYPOINT [ "java", "-jar" , "app.jar" ]
